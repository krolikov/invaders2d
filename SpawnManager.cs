﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

	public static Sprite[] logos;
	public static Sprite randomTexture;
	public static Vector2 spawnPosition;
	public static bool SpawnTopRowTrigger = false;
	private Object AlienPrefab;

	//Logo sizes and spacing
	public float horizontalSpacing = 0.7f;
	public float horizontalSize = 1.5f;
	public float sideMargin = 1.25f;
	public float verticalSpacing = 0.2f;
	public float verticalSize = 0.8f;
	public float topMargin = 0.2f;
	public float bottomMargin = 3.2f;

	void BuildTextureArray() {
		logos = Resources.LoadAll<Sprite>("LogoSprites");
	}
	
	void GetRandomTexture () {
		randomTexture = logos[Random.Range(0,56)];

	}      
	
	void FirstSpawn () {

		for (float spawnY = 2.8f; spawnY > -0.5f; spawnY -= 0.7f) {
			for (float spawnX = -3.6f; spawnX < 4.6f; spawnX += 1.8f) {
				//GetRandomTexture();
				spawnPosition.x = spawnX;
				spawnPosition.y = spawnY;
				GameObject EnemyClone = Instantiate(Resources.Load("AlienPrefab", typeof(GameObject)), spawnPosition, Quaternion.identity) as GameObject;
				//Debug.Log (EnemyClone.transform.position);
				EnemyClone.transform.localPosition = spawnPosition;
				//EnemyClone.renderer.material.mainTexture = randomTexture;

			}
		}
	}
	
	void SpawnTopRow() {
		SpawnTopRowTrigger = false;
		for (float spawnX = -3.6f; spawnX<4.6f; spawnX+=1.8f) {
			//GetRandomTexture();
			spawnPosition.x = spawnX;
			spawnPosition.y = 2.8f;
			GameObject EnemyClone = Instantiate(AlienPrefab, spawnPosition, Quaternion.identity) as GameObject;
			EnemyClone.transform.localPosition = spawnPosition;
		}
	}
	
	void SpawnListener () {
		if (SpawnTopRowTrigger == true) {
			SpawnTopRowTrigger = false;
			SpawnTopRow ();

		}
	}
		
	void Awake () {
		AlienPrefab = Resources.Load("AlienPrefab", typeof (GameObject));
		BuildTextureArray();
		FirstSpawn();
			
	}
		
	void Update () {
			SpawnListener();
	}
}