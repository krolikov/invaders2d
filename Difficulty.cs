﻿using UnityEngine;
using System.Collections;

public class Difficulty : MonoBehaviour {
	/*
	 * List of vars to be controlled by difficulty:
	 * - Enemy movement rate
	 * - Enemy bullet speed
	 * - Enemy shooting chance
	 * - Enemy shooting range
	 * - Score required for difficulty shift
	 * 
	 * Each var will have a starting value and an increment value
	 * Each increment value will be in turn incremented by a fixed amount with every difficulty shift
	 */

	public static float DEFAULT_ENEMY_MOVEMENT_DELAY = 120f;
	public static Vector2 DEFAULT_ENEMY_BULLET_SPEED = new Vector2(0f, 2f);
	public static int DEFAULT_ENEMY_SHOOTING_CHANCE = 30;
	public static float DEFAULT_ENEMY_SHOOTING_RANGE = 2f;

	public static float CURRENT_ENEMY_MOVEMENT_DELAY;
	public static Vector2 CURRENT_ENEMY_BULLET_SPEED;
	public static int CURRENT_ENEMY_SHOOTING_CHANCE;
	public static float CURRENT_ENEMY_SHOOTING_RANGE;

	public static float decrementMovementDelay = 10f;
	public static Vector2 incrementBulletSpeed = new Vector2(0f, 1f);
	public static int incrementShootingChance = 15;
	public static float incrementEnemyShootingRange = 1f;

	private int previousScoreAmount;
	private GameObject scoreObject;

	// Use this for initialization
	void Awake () {
		scoreObject = GameObject.Find ("ScoreKeeper");
		previousScoreAmount = scoreObject.GetComponent<ScoreKeeper> ().score;
		CURRENT_ENEMY_MOVEMENT_DELAY = DEFAULT_ENEMY_MOVEMENT_DELAY;
		CURRENT_ENEMY_BULLET_SPEED = DEFAULT_ENEMY_BULLET_SPEED;
		CURRENT_ENEMY_SHOOTING_CHANCE = DEFAULT_ENEMY_SHOOTING_CHANCE;
		CURRENT_ENEMY_SHOOTING_RANGE = DEFAULT_ENEMY_SHOOTING_RANGE;
	
	}

	void IncreaseDifficulty () {
		previousScoreAmount = scoreObject.GetComponent<ScoreKeeper> ().score;
		CURRENT_ENEMY_BULLET_SPEED += incrementBulletSpeed;
		CURRENT_ENEMY_MOVEMENT_DELAY -= decrementMovementDelay;
			if (CURRENT_ENEMY_SHOOTING_CHANCE < 100) {
				CURRENT_ENEMY_SHOOTING_CHANCE += incrementShootingChance;
			} else {
					CURRENT_ENEMY_SHOOTING_CHANCE = 100;
			}
		CURRENT_ENEMY_SHOOTING_RANGE += incrementEnemyShootingRange;
		}

	// Update is called once per frame
	void FixedUpdate () {
		if (scoreObject.GetComponent<ScoreKeeper> ().score - previousScoreAmount >= 5000) {
			IncreaseDifficulty ();
		}
	}
}
