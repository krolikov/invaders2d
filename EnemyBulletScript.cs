﻿using UnityEngine;
using System.Collections;

public class EnemyBulletScript : MonoBehaviour {

	public Vector2 speed = Difficulty.CURRENT_ENEMY_BULLET_SPEED;
	public Vector2 direction = new Vector2 (0f, -1f);
	private Vector2 movement;
	public static bool isInCameraView;

	// Use this for initialization
	void Awake () {
		Physics2D.IgnoreLayerCollision (8, 10);
		Physics2D.IgnoreLayerCollision (10, 11);

	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.GetComponent<PlayerScript>().isInvincible == false) {
			Destroy (gameObject, 0f);
			other.gameObject.GetComponent<PlayerScript> ().isDead = true;
		}
	}

	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.forward, 5f, Space.Self);
		movement = new Vector2 (speed.x * direction.x, speed.y * direction.y);

	}

	void FixedUpdate () {
		rigidbody2D.velocity = movement;
		isInCameraView = renderer.isVisible;
		if (isInCameraView == false) {
			Destroy (gameObject, 1);
		}
	}
}
