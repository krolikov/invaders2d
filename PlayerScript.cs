﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	private Object bullet;
	public float bulletOffset = 0.5f;
	private Vector2 bulletOrigin;
	//set player speed
	public Vector2 speed = new Vector2(2f, 2f);
	public float playerSpeed = 10f;
	public bool isDead;
	public float destructionTTL = 0.3f;
	public int playerLives = 2;	
	public bool canShoot = true;
	private Object flasherObject;
	public bool isInvincible;
	private AudioClip deathSFX; 

	//declare movement input cache
	public Vector2 movement;

	void Awake () {
		bullet = Resources.Load ("PlayerBullet", typeof(GameObject));
		isDead = false;
		isInvincible = false;
		Physics2D.IgnoreLayerCollision(8,9);
		flasherObject = this.GetComponent<SpriteFlasher>();
		deathSFX = Resources.Load<AudioClip>("playerDeath");
	}

	// Update is called once per frame
	void Update () {
		//float inputX = Input.GetAxis("Horizontal"); // get input for x axis
		bool inputShoot = Input.GetButtonDown ("Fire1");

		//calculate movement distance
		//movement = new Vector2 (speed.x * inputX, 0);
		//rigidbody2D.velocity = movement;

		if (!isDead) {
			transform.position = new Vector2 (Mathf.Clamp( transform.position.x + Input.GetAxis("Horizontal") * playerSpeed * (Time.deltaTime+0.05f),
			-5f,
			5f), -3f);
		}
		
		if (inputShoot && !isDead) {
			//GameObject bulletObject = GameObject.Find("PlayerBuller");
			if (canShoot) {
			//if (PlayerBulletScript.isInCameraView == false) {
				audio.Play ();
				bulletOrigin = new Vector2(transform.position.x, transform.position.y+bulletOffset);
				Instantiate(bullet, bulletOrigin, Quaternion.identity);
			}
		}
		if (isDead) {
			PlayerDestruction();
			if (playerLives > 0) {
				playerLives -= 1;
				PlayerRespawn();
			} else if (playerLives == 0) {
				GameObject gameManager = GameObject.Find("GameManager");
				gameManager.GetComponent<GameManager>().playerLost = true; 
			}
		}

	}

	void PlayerRespawn () {
		isDead = false;
		transform.position = new Vector2 (0f, -3f);
		this.GetComponent<SpriteFlasher>().StartFlashing();
	}

	void PlayerDestruction () {
		DrawFX ();
		audio.PlayOneShot(deathSFX);
		Vector2 deadPosition = new Vector2 (666f, 666f);
		transform.position = deadPosition;


		//Destroy (gameObject, destructionTTL);
	}
		
	void DrawFX () {
		Object explosion = Resources.Load ("PlayerExplosion", typeof(GameObject));
		ParticleSystem instanceFX = Instantiate (explosion, transform.position, Quaternion.identity) as ParticleSystem;
		Destroy (instanceFX, destructionTTL);
	}

	void FixedUpdate () {
		//rigidbody2D.velocity = movement;
	}
}
