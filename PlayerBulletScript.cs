﻿using UnityEngine;
using System.Collections;

public class PlayerBulletScript : MonoBehaviour {
	
	public Vector2 speed = new Vector2 (0f, 2f);
	public Vector2 direction = new Vector2 (0f, 1f);
	private Vector2 movement;
	public static bool isInCameraView = false;
	public static bool isPlayerBullet;
	private GameObject scoreObject;
	private GameObject playerObject;


	// Use this for initialization
	void Awake () {
		scoreObject = GameObject.Find ("ScoreKeeper");
		playerObject = GameObject.Find ("Player");
		playerObject.GetComponent<PlayerScript>().canShoot = false;
		Physics2D.IgnoreLayerCollision (9, 9);
		Physics2D.IgnoreLayerCollision (11, 9);
		Physics2D.IgnoreLayerCollision (11, 10);
		isInCameraView = true;
	}
	
	// Update is called once per frame
	void Update () {
		movement = new Vector2 (speed.x * direction.x, speed.y * direction.y);
	}
	
	void FixedUpdate () {
		rigidbody2D.velocity = movement;

		if (renderer.isVisible == false) {
			isInCameraView = false;
			playerObject.GetComponent<PlayerScript>().canShoot = true;
			Destroy (gameObject, 1);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		scoreObject.GetComponent<ScoreKeeper> ().score += 1000; 
		isInCameraView = false; 
		Destroy (gameObject, 0f);
		other.gameObject.GetComponent<EnemyControl> ().isDead = true;
		playerObject.GetComponent<PlayerScript>().canShoot = true;
	}
			
}
