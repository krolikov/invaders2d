﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	void Awake () {
	

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey("escape")) {
			Application.Quit();
		}

		if (Input.GetKey("return") || Input.GetKey("enter")) {
			PlayerPrefs.DeleteKey("Score");
			Application.LoadLevel("game");

		}
	
	}
}
