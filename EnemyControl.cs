﻿using UnityEngine;
using System.Collections;

public class EnemyControl : MonoBehaviour {

	//Movement speeds
	private Vector2 xSpeed = new Vector2 (0.4f, 0f);
	private Vector2 ySpeed = new Vector2 (0f, 0.7f);

	//Utility moving variables
	private string lastmove = "l2c"; //(l2c, c2r, r2c, c2l);
	private float movetimer = 0.0f;
	private int descendCounter = 0;

	private Vector2 raycastOrigin;
	private Transform playerTransform;
	public float raycastOffset = 0.4f;
	public float raycastLength = 0.5f;
	public float bulletOffset = 0.4f;
	private Object bulletPrefab;
	public bool isDead;
	private Object explosion;
	public float destructionTTL = 0.3f;

	private AudioClip shotSFX;
	private AudioClip deathSFX;

	//Spawner variables
	private Sprite randomTexture;
	private GameObject playerObject;
	private GameObject SpawnObject;
	private Vector2 newPosition;
	public Vector2 ManagerPosition;

	void MoveRight () {
		newPosition = new Vector2 (transform.position.x + xSpeed.x, transform.position.y);
		transform.position = newPosition;;
	}
	
	void MoveLeft () {
		newPosition = new Vector2 (transform.position.x - xSpeed.x, transform.position.y);
		transform.position = newPosition;
	}
	
	void MoveDown () {
		newPosition = new Vector2 (transform.position.x, transform.position.y - ySpeed.y);
		transform.position = newPosition;
		raycastOrigin = new Vector2 (transform.position.x, transform.position.y - raycastOffset);
	}
	
	
	void AlienMove () {
		movetimer = 0;
		descendCounter++;
		switch (lastmove) {
		case "l2c": //enemies are centered
			if (descendCounter < 5) {
				MoveRight();
				lastmove = "c2r";
			} else if (descendCounter >= 5) {
				descendCounter = 0;
				MoveDown();
				lastmove = "l2c";
				SpawnManager.SpawnTopRowTrigger = true;
			};
			break;
		case "c2r": //enemies are on the right
			MoveLeft ();
			lastmove = "r2c";
			break;
		case "r2c": //enemies are centered
			if (descendCounter < 5) {
				MoveLeft();
				lastmove = "c2l";
			} else if (descendCounter >= 5) {
				descendCounter = 0;
				MoveDown();
				lastmove = "r2c";
				SpawnManager.SpawnTopRowTrigger = true;
			};
			break;
		case "c2l": //enemies are on the left
			MoveRight();
			lastmove = "l2c";
			break;
		}      

	}
	
	
	void AlienShoot () {
		
		/* raycast to check if there's another alien right below. If not:
        check if alien is in range (horizontal distance to player). If it is:
        get a random number 0 - 100, shoot on <70. */  

		//Debug.Log ("AlienShoot called");
		RaycastHit2D raycastResult = Physics2D.Raycast (raycastOrigin, -Vector2.up, raycastLength);//, 1 << LayerMask.NameToLayer("Default")); //CHECK DOCS
		//Debug.Log ("Raycast result: " + raycastResult);

		Vector2 distance = playerTransform.position - transform.position;
				
		if (raycastResult.collider == null) {
						//Debug.Log ("Raycasted: no colliders");
						if (Mathf.Abs (distance.x) < Difficulty.CURRENT_ENEMY_SHOOTING_RANGE) {
								//Debug.Log ("Within shooting distance");
								if (Random.Range (0, 100) < Difficulty.CURRENT_ENEMY_SHOOTING_CHANCE) {
										//Debug.Log ("Shoot!"); //instantiate enemy bullet prefab;
										if (GameObject.Find("EnemyBullet") == null) {
						audio.PlayOneShot (shotSFX);
						Instantiate(bulletPrefab,transform.position, Quaternion.identity);
										}
								}
						}
				} else {
						//Debug.Log ("Raycaster: collider detected");	
				}
	}
	void GetRandomTexture () {
		randomTexture = SpawnManager.logos [Random.Range (0, 56)];

		}
	void Awake () {
		isDead = false;
		bulletPrefab = Resources.Load ("EnemyBullet", typeof(GameObject));
		playerObject = GameObject.Find ("Player");
		GetRandomTexture ();
		GetComponent<SpriteRenderer> ().sprite = randomTexture;
		raycastOrigin = new Vector2 (transform.position.x, transform.position.y - raycastOffset);
		playerTransform = playerObject.transform;
		InvokeRepeating ("AlienShoot", 3f, 3f);
		Physics2D.IgnoreLayerCollision(8,8);
		shotSFX = Resources.Load<AudioClip>("enemyShootSFX");
		deathSFX = Resources.Load<AudioClip>("enemyDeath");

	}

	void Destruction () {

		DrawFX ();
		Destroy (gameObject, destructionTTL);
		//spawn particle system
	}

	void DrawFX () {
		explosion = Resources.Load ("ExplosionFXPrefab", typeof(GameObject));
		ParticleSystem instanceFX = Instantiate (explosion, transform.position, Quaternion.identity) as ParticleSystem ;
		Destroy (instanceFX, destructionTTL);
		audio.clip = deathSFX;
		audio.PlayOneShot(deathSFX);
	}


	void Update () {
		if (isDead) {
			Destruction ();
		}

	}
	
	void FixedUpdate () {
		movetimer += 1;
		if (movetimer >= Difficulty.CURRENT_ENEMY_MOVEMENT_DELAY) {
			movetimer = 0;
			AlienMove();

		}

		if (transform.position.y <= playerTransform.position.y + 0.5f) {
			GameObject gameManager = GameObject.Find("GameManager");
			gameManager.GetComponent<GameManager>().playerLost = true; 
		}
	}
}
