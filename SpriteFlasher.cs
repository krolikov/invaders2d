﻿using UnityEngine;
using System.Collections;

public class SpriteFlasher : MonoBehaviour {

	// Use this for initialization
	public void StartFlashing () {
		this.GetComponent<PlayerScript>().isInvincible = true;
		StartCoroutine(FlashSprite());

	}
	
	// Update is called once per frame
	IEnumerator FlashSprite () {
		for (int i = 0;i<22;i++) {
			RendererVisibility(); 
			yield return new WaitForSeconds(0.06f);
		}
		this.GetComponent<PlayerScript>().isInvincible = false;
	}

	public void RendererVisibility() {
			if (renderer.enabled == true) {
				renderer.enabled = false;
			} else {
				renderer.enabled = true;
			}
	}
}
