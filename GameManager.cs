﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	private GameObject playerObject;
	private Sprite[] livesSprites;
	private Sprite currentLivesTexture;
	public bool playerLost = false;


	// Use this for initialization
	void Awake () {
		if (Application.platform == RuntimePlatform.OSXWebPlayer || 
		    Application.platform == RuntimePlatform.WindowsWebPlayer) {
			transform.position = new Vector2 (transform.position.x + 0.6f, transform.position.y);
		}
		playerObject = GameObject.Find("Player");
		livesSprites = Resources.LoadAll<Sprite>("Lives");

		//oneLife = Resources.Load("spr_OneLife", typeof(Sprite));
		//twoLives = Resources.Load("spr_TwoLives", typeof(Sprite));


	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("escape")) {
			Time.timeScale = 0;
		}
		if (playerObject.GetComponent<PlayerScript>().playerLives == 2) {
			currentLivesTexture = livesSprites[1];
		} else if (playerObject.GetComponent<PlayerScript>().playerLives == 1) {
			currentLivesTexture = livesSprites[0];
		} else {
			currentLivesTexture = null;
		}

		if (playerLost) {
			GameObject faderObject = GameObject.Find("ScreenFader");
			faderObject.GetComponent<ScreenFadeInOut>().SceneEnd();
		}
	}

	void OnGUI () {

		if (!playerLost) {
			//GUI.skin.font = Resources.Load<Font>("orangekid");

			GetComponent<SpriteRenderer>().sprite = currentLivesTexture;
			//GUI.Label (new Rect (15, Screen.height-110, 200, 30), "LIVES: "); 
		}

		if (Time.timeScale == 0) {
			if (Input.GetKey("escape") || Input.GetButtonDown("Fire1") || Input.GetKey("return") || Input.GetKey("enter")) {
				Time.timeScale = 1;
			}
		}

	}
}
