﻿using UnityEngine;
using System.Collections;

public class AnimateBackground : MonoBehaviour {

	public float scrollSpeed = 0f;
	
	// Update is called once per frame
	void Update () {
		renderer.material.mainTextureOffset = new Vector2 (0f, Time.time * scrollSpeed);
	}
}
