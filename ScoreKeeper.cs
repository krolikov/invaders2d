﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public int score;

	// Use this for initialization
	void Awake () {
		score = 666;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () {
		PlayerPrefs.SetInt("Score", score);
	}

	void OnGUI () {
		GameObject managerObject = GameObject.Find("GameManager");

		if (managerObject.GetComponent<GameManager>().playerLost == false) {
			guiText.text = "SCORE: " + score.ToString();
			//GUI.Label (new Rect (Screen.width-130, Screen.height-45, 200, 20), "SCORE: " + score.ToString()); 
		}
	}
}
