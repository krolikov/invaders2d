﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	private GameObject gameManager;
	private GameObject playerObject;
	private Texture2D fadeTexture;

	private float fadeSpeed = 0.2f;
	private int drawDepth = -1000;
	private float alpha;
	private int fadeDirection = -1;

	private bool fadeIn;
	private bool fadeOut;



	// Use this for initialization
	void Start () {
		fadeTexture = Resources.Load<Texture2D>("blackTexture");
		fadeIn = true;
	}
	// Update is called once per frame
	void OnGUI () {

		if (fadeIn) {
			while (alpha != 0) {
				alpha += fadeDirection * fadeSpeed * Time.deltaTime;  
				alpha = Mathf.Clamp01(alpha);   
				Color thisColor = GUI.color;
				thisColor.a  = alpha;
				GUI.color = thisColor;
				GUI.depth = drawDepth;
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
			}
			fadeIn = false;
		}

		if (fadeOut) {
			while (alpha < 0) {
				alpha += fadeDirection * fadeSpeed * Time.deltaTime;  
				alpha = Mathf.Clamp01(alpha);   
				Color thisColor = GUI.color;
				thisColor.a  = alpha;
				GUI.color = thisColor;
				GUI.depth = drawDepth;
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
			}
			fadeOut = false;
		}	
	}
}
